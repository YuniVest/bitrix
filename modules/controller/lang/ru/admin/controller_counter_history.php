<?
$MESS ['CTRL_CONTER_HIST_TITLE'] = "История правок команды счётчика";
$MESS ['CTRL_CONTER_HIST_BACK'] = "Счётчик";
$MESS ['CTRL_CONTER_HIST_BACK_TITLE'] = "Вернуться к редактированию счётчика";
$MESS ['CTRL_CONTER_HIST_COUNTER_ID'] = "Идентификатор счётчика";
$MESS ['CTRL_CONTER_HIST_TIMESTAMP_X'] = "Время";
$MESS ['CTRL_CONTER_HIST_USER_ID'] = "Пользователь";
$MESS ['CTRL_CONTER_HIST_COMMAND'] = "Команда";
$MESS ['CTRL_CONTER_HIST_COMMAND_FROM'] = "Команда до";
$MESS ['CTRL_CONTER_HIST_COMMAND_TO'] = "Команда после";
$MESS ['CTRL_CONTER_HIST_NAVSTRING'] = "Запись";
?>